git-lfs
=======

Documentation: https://gitlab.phys.ethz.ch/help/topics/git/lfs/index

usage
-----

Enable **Large File Storage** in: Gitlab > Group > Settings > General > Projects in this group can use Git LFS:

![git lfs settings](img/git-lfs-settings.png "git lfs settings")

Install `git-lfs` (pre-installed on managed Linux workstations):

```bash
apt install git-lfs
```

In the git repo clone:

```bash
git lfs track "*.png"
git lfs track "*.jpg"
git add .gitattributes
git commit -m 'Store png and jpg in git lfs'
```

From now on the lfs tracked files will be uploaded as lfs objects:

```bash
+rda:~/git/test/git-lfs[±]$ git add -A
+rda:~/git/test/git-lfs[±]$ git status
On branch master
Your branch is up to date with 'origin/master'.

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        modified:   README.markdown
        new file:   img/git-lfs-settings.png
+rda:~/git/test/git-lfs[±]$ git commit -m 'Document git lfs usage'
+rda:~/git/test/git-lfs[+]$ git push
Locking support detected on remote "origin". Consider enabling it with:
  $ git config lfs.https://gitlab.phys.ethz.ch/isgphys/git-lfs.git/info/lfs.locksverify true
Uploading LFS objects: 100% (2/2), 318 KB | 0 B/s, done.
Enumerating objects: 9, done.
Counting objects: 100% (9/9), done.
Delta compression using up to 8 threads
Compressing objects: 100% (7/7), done.
Writing objects: 100% (7/7), 843 bytes | 843.00 KiB/s, done.
Total 7 (delta 0), reused 0 (delta 0), pack-reused 0
To gitlab.phys.ethz.ch:isgphys/git-lfs.git
   c65d50b..e73062e  master -> master
```

users
-----

![compi cat](img/compi-cat.png "compi cat")
![hacker cat](img/hacker-cat.jpg "hacker cat")
